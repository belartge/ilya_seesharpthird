﻿using System;

namespace ThirdLab
{
    public abstract class Currency
    {
        protected double CoefficientToRuble = 1.0;

        public virtual double Convert(double money)
        {
            return money * CoefficientToRuble;
        }

        public virtual void Print()
        {
            Console.WriteLine($"Перевод из валюты Х к рублю по курсу {CoefficientToRuble}");
        }
    }

    public class Dollar : Currency
    {
        private Double _money;
        private bool _wasSet = false;

        public Dollar(double exchangeRate = 63.3)
        {
            CoefficientToRuble = exchangeRate;
        }

        public override double Convert(double money)
        {
            _money = money;
            _wasSet = true;
            return CoefficientToRuble * money;
        }

        public override void Print()
        {
            Console.WriteLine($"Перевод от доллара к рублю по курсу {CoefficientToRuble}");
            if (_wasSet)
            {
                Console.WriteLine($"\tПереведено {_money} -> {Convert(_money)}");
            }
        }
    }

    public class Euro : Currency
    {
        private Double _money;
        private bool _wasSet = false;

        public Euro(double exchangeRate = 75.5)
        {
            CoefficientToRuble = exchangeRate;
        }

        public override double Convert(double money)
        {
            _money = money;
            _wasSet = true;
            return CoefficientToRuble * money;
        }

        public override void Print()
        {
            Console.WriteLine($"Перевод от евро к рублю по курсу {CoefficientToRuble}");
            if (_wasSet)
            {
                Console.WriteLine($"\tПереведено {_money} -> {Convert(_money)}");
            }
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            Euro euroConverter = new Euro();
            double money = 100;
            Console.WriteLine($"Перевод {money} евро в рубли: {euroConverter.Convert(money)}");
            euroConverter.Print();
            
            Console.WriteLine("------------------------");
            
            Dollar dollarConverter = new Dollar();
            money = 100;
            Console.WriteLine($"Перевод {money} долларов в рубли: {dollarConverter.Convert(money)}");
            dollarConverter.Print();
            
            Console.WriteLine("Нажмите Enter для выхода...");
            Console.ReadLine();
        }
    }
}